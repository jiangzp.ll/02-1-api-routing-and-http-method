package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
@AutoConfigureMockMvc
class RoutingAndHttpControllerIntegratedTest {
    @Autowired
    private MockMvc mockMvc;

    //2.1 Path Variable 的匹配
    @Test
    void should_get_return_userId_is_2_message() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/2/books"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("The book for user 2"));
    }

    @Test
    void should_get_return_userId_is_23_message() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/23/books"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("The book for user 23"));
    }

    //2.2 Path Variable 大小写
    @Test
    void should_test_different_case_for_PathVariable() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/test"))
        .andExpect(MockMvcResultMatchers.status().is(200))
        .andExpect(MockMvcResultMatchers.content().string("The name of this book is test"));
    }

    //2.3 优先级测试
    @Test
    void should_test_priority_for_url_1() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/segments/good"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("segments/good"));
    }

    @Test
    void should_test_priority_for_url_2() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/segments/good"))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    //2.4-2.6通配符
    @Test
    void should_return_200_when_hava_two_char_macth() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/segments/get/bbad"))
                .andExpect(MockMvcResultMatchers.status().is(404));
    }

    @Test
    void should_return_404_when_hava_two_no_macth() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/segments/get/ad"))
                .andExpect(MockMvcResultMatchers.status().is(404));
    }

    @Test
    void should_return_200_when_hava_one_segment() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/wildcards/anything"))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }


    @Test
    void should_return_200_when_hava_one_segment_in_intermediate() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/anything/wildcards"))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    void should_return_404_when_no_segment_in_intermediate() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/wildcard/before/after"))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    void should_return_200_when_match_segment_in_intermediate() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/wildcard/good/after"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("mathch *o*"));
    }

    @Test
    void should_return_200_when_match_double_segment_in_intermediate() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/wildcard/good/before/after/double"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("mathch **"));
    }

    //2.7 正则表达式
    @Test
    void should_test_regex() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/regex/1"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("Your id is 1"));
    }

    //2.8 Query String
    @Test
    void should_test_Query_String() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/books?bookName=springboot"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("The name of this book is springboot"));
    }
}