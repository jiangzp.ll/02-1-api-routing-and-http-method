package com.twuc.webApp;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RoutingAndHttpControllerTest {

    //2.1 Path Variable 的匹配
    @Test
    void should_get_return_userId_is_2_message() {
        RoutingAndHttpController rahc = new RoutingAndHttpController();
        String result = rahc.getMessage(2);
        assertEquals("The book for user 2", result);
    }

    @Test
    void should_get_return_userId_is_23_message() {
        RoutingAndHttpController rahc = new RoutingAndHttpController();
        String result = rahc.getMessage(23);
        assertEquals("The book for user 23", result);
    }

    //2.2 Path Variable 大小写
    @Test
    void should_test_different_case_for_PathVariable() {
        RoutingAndHttpController rahc = new RoutingAndHttpController();
        String result = rahc.getDifferentCaseMessage("test");
        assertEquals("The name of this book is test", result);
    }

    //2.3 优先级测试
    @Test
    void should_test_priority_url_unchanged() {
        RoutingAndHttpController rahc = new RoutingAndHttpController();
        String result = rahc.getPriority_1("good");
        assertEquals("segments/good", result);
    }

    @Test
    void should_test_priority_url_changed() {
        RoutingAndHttpController rahc = new RoutingAndHttpController();
        String result = rahc.getPriority_2("good");
        assertEquals("segments/{good}", result);
    }

    //2.7 正则表达式
    @Test
    void should_test_regex() {
        RoutingAndHttpController rahc = new RoutingAndHttpController();
        String result = rahc.getRegex(1);
        assertEquals("Your id is 1", result);
    }

    //2.8 Query String
    @Test
    void should_test_Query_String() {
        RoutingAndHttpController rahc = new RoutingAndHttpController();
        String result = rahc.getQueryString("springboot");
        assertEquals("The name of this book is springboot", result);
    }
}