package com.twuc.webApp;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class RoutingAndHttpController {
    @GetMapping("/users/{userId}/books")
    public String getMessage(@PathVariable Integer userId) {
        return String.format("The book for user %d", userId);
    }

    @GetMapping("/{test}")
    public String getDifferentCaseMessage(@PathVariable("test") String Test) {
        return String.format("The name of this book is %s", Test);
    }

    @GetMapping("/segments/good")
    public String getPriority_1(String good) {
        return "segments/good";
    }

    @GetMapping("/segments/{good}")
    public String getPriority_2(@PathVariable String good) {
        return good;
    }

    @GetMapping("/segments/get/?ad")
    public String getBadSegment() {
        return "successful";
    }

    @GetMapping("/wildcards/*")
    public String getWildcards() {
        return "successful";
    }

    @GetMapping("/*/wildcards")
    public String getAnyWildcards() {
        return "successful";
    }

    @GetMapping("/wildcard/before/*/after")
    public String getBeforeAfterWildcards() {
        return "successful";
    }

    @GetMapping("/wildcard/*o*/after")
    public String getAnyAfterWildcards() {
        return "mathch *o*";
    }

    @GetMapping("/wildcard/**/after/double")
    public String getDoubleWildcards() {
        return "mathch **";
    }

    @GetMapping("/regex/{userId:[\\d]{1}}")
    public String getRegex(@PathVariable Integer userId) {
        return String.format("Your id is %d",userId);
    }

    @GetMapping("/books")
    public String getQueryString(@RequestParam("bookName") String bookName) {
        return String.format("The name of this book is %s", bookName);
    }
}
